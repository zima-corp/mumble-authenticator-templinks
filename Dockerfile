FROM python:3.12-alpine AS build
ENV PATH="/root/.local/bin:$PATH"
ENV BUILD_DIR="/mumble-authenticator-templinks"
WORKDIR ${BUILD_DIR}
COPY . ${BUILD_DIR}
RUN \
  set -eux; \
  apk upgrade --no-cache ; \
  apk add --no-cache --upgrade \
  curl \
  ; \
  curl -sSL https://install.python-poetry.org | python3 -; \
  poetry build

FROM python:3.12-alpine
ARG UID=10000
ARG GID=10000
ARG APP_NAME="app"
ENV MA__ICE_CLIENT__BIND_ADDRESS=0.0.0.0
ENV MA__ICE_CLIENT__PORT=13004
ENV MA__PROMETHEUS__ENABLED=true
ENV MA__PROMETHEUS__PORT=8000
COPY --from=build /mumble-authenticator-templinks/dist/*.whl /tmp/
COPY docker-entrypoint.sh /
RUN set -eux; \
  chmod +x /docker-entrypoint.sh; \
  mkdir /data; \
  addgroup -g ${GID} ${APP_NAME}; \
  adduser -D -G ${APP_NAME} -u ${UID} -h /data ${APP_NAME}; \
  chown -R ${APP_NAME}:${APP_NAME} /data; \
  apk upgrade --no-cache ; \
  apk add --no-cache --virtual .build-deps \
  bzip2-dev \
  g++ \
  openssl-dev \
  ; \
  pip install --no-color --disable-pip-version-check --no-cache-dir \
  /tmp/*.whl \
  ;\
  rm -rf /tmp/*.whl; \
  apk del --no-network .build-deps; \
  apk add --no-cache --upgrade \
  libstdc++ \
  su-exec \
  tini \
  ;

EXPOSE ${MA__ICE_CLIENT__PORT}
EXPOSE ${MA__PROMETHEUS__PORT}
VOLUME /data
WORKDIR /data
ENTRYPOINT ["tini", "--", "/docker-entrypoint.sh"]
CMD ["authenticator"]
