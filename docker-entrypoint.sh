#!/bin/sh -eux

# allow the container to be started with `--user`
if [ "$1" = 'authenticator' -a "$(id -u)" = '0' ]; then
	find . \! -user app -exec chown app '{}' +
	exec su-exec app "$0" "$@"
fi

exec "$@"
